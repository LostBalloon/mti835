using UnityEngine;
using System.Collections;

//[RequireComponent (typeof (CharacterController))]
//[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    //public float moveSpeed = 5.5f;
    public float turnSpeed = 2.0f;
    //CharacterController c;
	
    //// Update is called once per frame
    //void Update () {
    //    CharacterController c = GetComponent<CharacterController>();
    //    float speed = moveSpeed * Input.GetAxis("Vertical");    // get forward/back input

    //    transform.Rotate(0.0f, Input.GetAxis("Horizontal") * turnSpeed, 0.0f);  // get left/right input
    //    c.SimpleMove(transform.forward * speed);  // Moves the character with given speed.
    //}

    //------------------------ NEW since animations ----------------------------
    public float turnSmooting = 15.0f;
    public float speedDampTime = 0.1f;

    private Animator anim;
    float h;
    float v;
    bool sneak;

    void Awake()
    {
        anim = gameObject.GetComponent<Animator>();
        //c = gameObject.GetComponent<CharacterController>();
    }

    void Update()
    {
        AudioManagement();
        
    }

    void FixedUpdate()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        sneak = Input.GetButton("Sneak");
        MouvementManagement(h, v, sneak);
    }

    void MouvementManagement(float horizontal, float vertical, bool sneaking)
    {
        anim.SetBool("Sneak", sneaking);
        transform.Rotate(0.0f, horizontal * turnSpeed, 0.0f);  // get left/right input

        if (vertical != 0.0f)
        {
            if (vertical > 0)
            {
                anim.SetFloat("Speed", 5.5f, speedDampTime, Time.deltaTime);
            }
            else
            {
            //    anim.SetFloat("Speed", 1.0f, speedDampTime, Time.deltaTime);
            }
        }
        else
        {
            anim.SetFloat("Speed", 0.0f);
        }
    }

    void Rotating(float horizontal, float vertical)
    {
        //Debug.Log("h " + horizontal);
        //Debug.Log("v " + vertical);
        Debug.Log(transform.forward);
        //Vector3 targetDirection = new Vector3(horizontal, 0.0f, vertical);
        //Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
        //Quaternion newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, turnSmooting * Time.deltaTime);

        //rigidbody.MoveRotation(newRotation);

        //CharacterController c = GetComponent<CharacterController>();
        //float speed = moveSpeed * Input.GetAxis("Vertical");    // get forward/back input

        
        //c.SimpleMove(transform.forward * speed);  // Moves the character with given speed.
    }

    void AudioManagement()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Locomotion"))
        {
            if (!audio.isPlaying)
            {
                audio.Play();
            }
        }
        else
        {
            audio.Stop();
        }
    }
}