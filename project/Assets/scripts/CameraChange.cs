using UnityEngine;
using System.Collections;

public class CameraChange : MonoBehaviour {

    public Transform first; // transform information for first person view. (behind head)
    public Transform third; // transform information for thrid person view.
    public Transform realFPView;  // final first person view

    public float transitionTime = 1.0f;

    private bool isFirstPerson = false;   // camera is by default set to third person in the player asset.
    private float timeElapsed = 0.0f;   // time elapsed since transition started

    void Start()
    {
        timeElapsed = transitionTime + 1;   // make sure time elapsed is greater tha transition time on start.
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Jump"))
        {
            if(timeElapsed > transitionTime)
            {
                StartCoroutine(ChangePerspective());
            }
        }
	}

    IEnumerator ChangePerspective()
    {
        timeElapsed = 0.0f;
        float step;

        if (isFirstPerson)
        {
            transform.position = first.position;
            yield return null;

            while (timeElapsed < transitionTime)
            {
                timeElapsed += Time.deltaTime;
                step = timeElapsed / transitionTime;

                transform.position = Vector3.Lerp(first.position, third.position, step);
                transform.rotation = Quaternion.Lerp(first.rotation, third.rotation, step);
                yield return null;
            }

            isFirstPerson = false;
        }
        else
        {
            while (timeElapsed < transitionTime)
            {
                timeElapsed += Time.deltaTime;
                step = timeElapsed / transitionTime;

                transform.position = Vector3.Lerp(third.position, first.position, step);
                transform.rotation = Quaternion.Lerp(third.rotation, first.rotation, step);
                yield return null;
            }

            transform.position = realFPView.position;
            isFirstPerson = true;
        }

        yield return null;
    }

    public bool IsFirstPerson()
    {
        return isFirstPerson;
    }

    public void SetFirstPerson()
    {
        isFirstPerson = true;
        transform.position = realFPView.position;
        transform.rotation = realFPView.rotation;
    }

    public bool GetIsFirstPerson()
    {
        return isFirstPerson;
    }
}