using UnityEngine;
using System.Collections;

public class StaticCameraSwap : MonoBehaviour {

    public Camera playerCamera;
    public Camera staticCamera;

    void OnTriggerEnter(Collider other)
    {
        playerCamera.enabled = false;
        staticCamera.enabled = true;
    }

    void OnTriggerExit(Collider other)
    {
        staticCamera.enabled = false;
        playerCamera.enabled = true;
    }
}