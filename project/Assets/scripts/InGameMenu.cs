using UnityEngine;
using System.Collections;

public class InGameMenu : MonoBehaviour {

    private bool showMenu = false;

    void Awake()
    {
        if (SaveGame.hasLoaded)
        {
            GameObject player = GameObject.FindWithTag("Player");
            player.transform.position = SaveGame.saveData.playerPosition.transform.position;
            player.transform.rotation = SaveGame.saveData.playerPosition.transform.rotation;

            if (SaveGame.saveData.cameraInFirstPerson)
                (player.GetComponentInChildren<CameraChange>()).SetFirstPerson();

            if (!SaveGame.saveData.cubeInScene)
                Destroy(GameObject.Find("Object_cube"));

            if (!SaveGame.saveData.cylinderInScene)
                Destroy(GameObject.Find("Object_Cylinder"));

            if (!SaveGame.saveData.sphereInScene)
                Destroy(GameObject.Find("Object_Sphere"));

            SaveGame.hasLoaded = false;
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("GameMenu"))
        {
            showMenu = !showMenu;
        }
    }

    void OnGUI()
    {
        if (showMenu)
        {
            if (GUI.Button(new Rect(10, 10, 100, 30), "Restart Level"))
                Application.LoadLevel("scene1");

            if (GUI.Button(new Rect(10, 50, 100, 30), "Save Level"))
            {
                SaveGame.Save();
            }

            if (GUI.Button(new Rect(10, 90, 100, 30), "Main Menu"))
                Application.LoadLevel("Main menu");
        }
    }
}
