﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour {

    public Transform target;

    private CameraChange cc;
    private bool isfirstperson;
    private Vector3 lastMousePosition;
    private Vector3 PositionDifferential;

    void Start()
    {
        cc = gameObject.GetComponent<CameraChange>();
        lastMousePosition = Input.mousePosition;
    }
	
	// Update is called once per frame
	void Update ()
    {
        isfirstperson = cc.GetIsFirstPerson();
        PositionDifferential = Input.mousePosition - lastMousePosition;
        lastMousePosition = Input.mousePosition;

        if (!isfirstperson && Input.GetMouseButton(1))
        {
            transform.LookAt(target);
            transform.RotateAround(target.position, Vector3.up, PositionDifferential.x);
            transform.RotateAround(target.position, Vector3.right, PositionDifferential.y);
        }
	}
}
