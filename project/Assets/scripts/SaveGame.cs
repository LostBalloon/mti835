using UnityEngine;
using System.Collections;

using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
 
using System;
using System.Runtime.Serialization;
using System.Reflection;

[Serializable ()]
public class SaveGame : ISerializable {

    public static string currentFilePath = "save.dat";

    public static SaveGame saveData;
    public static bool hasLoaded = false;

	//============ DATA TO SERIALIZE =========== This is set to the default game state
    public GameObject playerPosition = new GameObject();
    public bool cubeInScene = true;
    public bool sphereInScene = true;
    public bool cylinderInScene = true;
    public bool cameraInFirstPerson = false;
    //==========================================

    public SaveGame()
    {
    }

    // required by ISerializable
    public SaveGame(SerializationInfo info, StreamingContext context)
    {
        playerPosition.transform.position = new Vector3((float)info.GetValue("playerPositionX", typeof(float))
            ,(float)info.GetValue("playerPositionY", typeof(float))
            ,(float)info.GetValue("playerPositionZ", typeof(float)));

        playerPosition.transform.rotation = new Quaternion((float)info.GetValue("playerRotationX", typeof(float))
            , (float)info.GetValue("playerRotationY", typeof(float))
            , (float)info.GetValue("playerRotationZ", typeof(float))
            , (float)info.GetValue("playerRotationW", typeof(float)));

        cubeInScene = (bool)info.GetValue("cubeInScene", typeof(bool));
        sphereInScene = (bool)info.GetValue("sphereInScene", typeof(bool));
        cylinderInScene = (bool)info.GetValue("cylinderInScene", typeof(bool));
        cameraInFirstPerson = (bool)info.GetValue("cameraInFirstPerson", typeof(bool));
    }

    // required by ISerializable
    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("playerPositionX", playerPosition.transform.position.x, typeof(float));
        info.AddValue("playerPositionY", playerPosition.transform.position.y, typeof(float));
        info.AddValue("playerPositionZ", playerPosition.transform.position.z, typeof(float));
        info.AddValue("playerRotationW", playerPosition.transform.rotation.w, typeof(float));
        info.AddValue("playerRotationX", playerPosition.transform.rotation.x, typeof(float));
        info.AddValue("playerRotationY", playerPosition.transform.rotation.y, typeof(float));
        info.AddValue("playerRotationZ", playerPosition.transform.rotation.z, typeof(float));
        info.AddValue("cubeInScene", cubeInScene);
        info.AddValue("sphereInScene", sphereInScene);
        info.AddValue("cylinderInScene", cylinderInScene);
        info.AddValue("cameraInFirstPerson", cameraInFirstPerson);
    }

    private void CollectData()
    {
        GameObject player = GameObject.FindWithTag("Player");
        playerPosition = player;
        cameraInFirstPerson = (player.GetComponentInChildren<CameraChange>()).IsFirstPerson();

        if (GameObject.Find("Object_cube") != null)
            cubeInScene = true;
        else
            cubeInScene = false;

        if (GameObject.Find("Object_Sphere") != null)
            sphereInScene = true;
        else
            sphereInScene = false;

        if (GameObject.Find("Object_Cylinder") != null)
            cylinderInScene = true;
        else
            cylinderInScene = false;
    }

    public static void Save()
    {
        Save(currentFilePath);
    }

    public static void Save(string filePath)
    {
        SaveGame data = new SaveGame();
        data.CollectData();

        Stream stream = File.Open(filePath, FileMode.Create);
        BinaryFormatter bformatter = new BinaryFormatter();
        bformatter.Binder = new VersionDeserializationBinder();
        bformatter.Serialize(stream, data);
        stream.Close();
    }

    public static void Load() 
    {
        Load(currentFilePath);
    }

    public static void Load(string filePath)
    {
        SaveGame data = new SaveGame();
        Stream stream = File.Open(filePath, FileMode.Open);
        BinaryFormatter bformatter = new BinaryFormatter();
        bformatter.Binder = new VersionDeserializationBinder();
        data = (SaveGame)bformatter.Deserialize(stream);
        stream.Close();

        saveData = data;
        hasLoaded = true;
    }
}

// === This is required to guarantee a fixed serialization assembly name, which Unity likes to randomize on each compile
// Do not change this
public sealed class VersionDeserializationBinder : SerializationBinder
{
    public override Type BindToType(string assemblyName, string typeName)
    {
        if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
        {
            Type typeToDeserialize = null;

            assemblyName = Assembly.GetExecutingAssembly().FullName;

            // The following line of code returns the type. 
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return typeToDeserialize;
        }

        return null;
    }
}