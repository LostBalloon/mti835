﻿using UnityEngine;
using System.Collections;

public class BatteryRecharge : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        other.GetComponentInChildren<Flashlight>().SetIntensity(3.0f);
        Destroy(gameObject);
    }
}
