using UnityEngine;
using System.Collections;

public class MiniMap : MonoBehaviour {

    Transform player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.position = player.position + Vector3.up * 10;
	}
}