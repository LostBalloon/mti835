using UnityEngine;
using System.Collections;

public class ObjectSelection : MonoBehaviour {

    void OnMouseEnter()
    {
        renderer.material.shader = Shader.Find("Outlined/Silhouetted Diffuse");
        renderer.material.SetColor("_OutlineColor", Color.green);
        renderer.material.SetFloat("_Outline", 0.003f);
    }

    void OnMouseExit()
    {
        renderer.material.shader = Shader.Find("Diffuse");
    }

    void OnMouseUpAsButton()
    {
        Destroy(gameObject);
    }
}