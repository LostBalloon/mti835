using UnityEngine;
using System.Collections;

public class Flashlight : MonoBehaviour {

    
    public float drainSpeed = 1.0f;
    private bool flashlightOpen = false;
    private float intensity = 3;
	
	// Update is called once per frame
	void Update () {
        if (flashlightOpen)
        {
            gameObject.light.intensity -= Time.deltaTime * 0.5f * drainSpeed;
        }

        if (Input.GetButtonDown("Flashlight"))
        {
            if (flashlightOpen)
            {
                gameObject.light.enabled = false;
            }
            else
            {
                gameObject.light.enabled = true;
            }

            flashlightOpen = !flashlightOpen;
        }
	}

    public void SetIntensity(float i)
    {
        gameObject.light.intensity = i;
    }
}
