using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 100, 30), "New Game"))
            Application.LoadLevel("scene1");

        if (GUI.Button(new Rect(10, 50, 100, 30), "Load Level"))
        {
            SaveGame.Load();
            GameObject.DontDestroyOnLoad(SaveGame.saveData.playerPosition);
            Application.LoadLevel("scene1");
        }

        if (GUI.Button(new Rect(10, 90, 100, 30), "Exit Game"))
            Application.Quit();
    }
}